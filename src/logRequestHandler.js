const { LogLevel } = require('configurapi');
const redactor = require('mask-json');
let qs = require('querystring');

module.exports = function logRequestHandler(event, excludedProperties = [])
{
    let payload = event.payload;
    let params = event.params;

    if(excludedProperties)
    {
        let isUrlEncodedFormPayload = event.request.headers && event.request.headers['content-type'] === 'application/x-www-form-urlencoded'

        if(isUrlEncodedFormPayload)
        {
            payload = Object.assign({}, qs.parse((payload || '').toString('utf-8')))
        }

        payload = require('mask-json')(excludedProperties, {replacement:'*****'})(payload);
        params = require('mask-json')(excludedProperties, {replacement:'*****'})(params);

        if(isUrlEncodedFormPayload)
        {
            payload = qs.stringify(payload);
        }
    }

    this.emit(LogLevel.Trace, `Request - ${event.name} - ${event.identity ? event.identity.id : ''} - ${params ? JSON.stringify(params) : ''} - ${payload ? (payload instanceof Buffer ? '[Buffer]' : JSON.stringify(payload)) : ''}`);

    this.continue();
};
