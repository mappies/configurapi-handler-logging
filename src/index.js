module.exports = {
    logRequestHandler: require('./logRequestHandler'),
    logResponseHandler: require('./logResponseHandler'),
};
