const assert = require('chai').assert;
const sinon = require('sinon');
const helper = require('./helper');
const Configurapi = require('configurapi');

const logRequestHandler = require('../src/logRequestHandler');

describe('logRequestHandler', () =>
{
    it("Log a request properly", async () =>
    {
        let context = helper.createContext();

        let ev = sinon.mock(Configurapi.Event);

        ev.request = sinon.mock(Configurapi.Request);
        ev.response = sinon.mock(Configurapi.Response);
        ev.name = 'event_name'
        ev.payload = {"payload": true};

        logRequestHandler.apply(context, [ev]);

        assert.isTrue(context.emit.called);
        assert.isTrue(context.emit.calledWith(Configurapi.LogLevel.Trace, `Request - event_name -  -  - {"payload":true}`));
    });

    it("Log a request properly - exclude the payload", async () =>
    {
        let context = helper.createContext();

        let ev = sinon.mock(Configurapi.Event);

        ev.request = sinon.mock(Configurapi.Request);
        ev.response = sinon.mock(Configurapi.Response);
        ev.name = 'event_name'
        ev.payload = {"payload": true, "secret": "abcd", "password": "1234", "public": "data"};

        logRequestHandler.apply(context, [ev, ["secret", "password"]]);

        assert.isTrue(context.emit.called);
        assert.isTrue(context.emit.calledWith(Configurapi.LogLevel.Trace, `Request - event_name -  -  - {"payload":true,"secret":"*****","password":"*****","public":"data"}`));
    });

    it("Log a request properly - exclude the params", async () =>
    {
        let context = helper.createContext();

        let ev = sinon.mock(Configurapi.Event);

        ev.request = sinon.mock(Configurapi.Request);
        ev.response = sinon.mock(Configurapi.Response);
        ev.name = 'event_name'
        ev.params = {"params": true, "id_token": "abcd", "password": "1234", "public": "data"};

        logRequestHandler.apply(context, [ev, ["id_token", "password"]]);

        assert.isTrue(context.emit.called);
        assert.isTrue(context.emit.calledWith(Configurapi.LogLevel.Trace, `Request - event_name -  - {"params":true,"id_token":"*****","password":"*****","public":"data"} - `));
    });

    it("Log a request properly - exclude the params and payload", async () =>
    {
        let context = helper.createContext();

        let ev = sinon.mock(Configurapi.Event);

        ev.request = sinon.mock(Configurapi.Request);
        ev.response = sinon.mock(Configurapi.Response);
        ev.name = 'event_name'
        ev.params = {"params": true, "id_token": "abcd"};
        ev.payload = {"secret": "abcd", "public": "data"};

        logRequestHandler.apply(context, [ev, ["id_token", "secret"]]);

        assert.isTrue(context.emit.called);
        assert.isTrue(context.emit.calledWith(Configurapi.LogLevel.Trace, `Request - event_name -  - {"params":true,"id_token":"*****"} - {"secret":"*****","public":"data"}`));
    });

    it("Log a request properly - exclude the form payload", async () =>
    {
        let context = helper.createContext();

        let ev = sinon.mock(Configurapi.Event);

        ev.request = sinon.mock(Configurapi.Request);
        ev.request['headers'] = {'content-type': 'application/x-www-form-urlencoded'}
        ev.response = sinon.mock(Configurapi.Response);
        ev.name = 'event_name'
        ev.payload = "utf8=%E2%9C%93&authenticity_token=zR6kuuWwxhSSebKLfJlenFr8pYHBSJ%2FBnprPKnR2SimPNcjQvPWRXPkSxv07%2FwfzK6bk5blwz6nswPZoFScATQ%3D%3D&id_token=eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImtp&somethingelse=12";

        logRequestHandler.apply(context, [ev, ["id_token", "authenticity_token"]]);

        assert.isTrue(context.emit.called);
        assert.isTrue(context.emit.calledWith(Configurapi.LogLevel.Trace, `Request - event_name -  -  - "utf8=%E2%9C%93&authenticity_token=*****&id_token=*****&somethingelse=12"`));
    });

    it("Log a request properly - include the payload", async () =>
    {
        let context = helper.createContext();

        let ev = sinon.mock(Configurapi.Event);

        ev.request = sinon.mock(Configurapi.Request);
        ev.response = sinon.mock(Configurapi.Response);
        ev.name = 'event_name'
        ev.payload = {"payload": true};

        logRequestHandler.apply(context, [ev, true]);

        assert.isTrue(context.emit.called);
        assert.isTrue(context.emit.calledWith(Configurapi.LogLevel.Trace, `Request - event_name -  -  - {"payload":true}`));
    });

    it("Log a request properly - undefined payload", async () =>
    {
        let context = helper.createContext();

        let ev = sinon.mock(Configurapi.Event);

        ev.request = sinon.mock(Configurapi.Request);
        ev.response = sinon.mock(Configurapi.Response);
        ev.name = 'event_name'
        ev.payload = undefined;

        logRequestHandler.apply(context, [ev, true]);

        assert.isTrue(context.emit.called);
        assert.isTrue(context.emit.calledWith(Configurapi.LogLevel.Trace, `Request - event_name -  -  - `));
    });
});
