const sinon = require('sinon');

module.exports =  {
    createContext()
    {
        return {
                    'continue': sinon.spy(),
                    'complete': sinon.spy(),
                    'catch': sinon.spy(),
                    'emit': sinon.spy()
                };
    }
};