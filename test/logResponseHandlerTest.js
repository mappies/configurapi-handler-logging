const assert = require('chai').assert;
const sinon = require('sinon');
const helper = require('./helper');
const Configurapi = require('configurapi');

const logResponseHandler = require('../src/logResponseHandler');

describe('logResponseHandler', () =>
{
    it("Log a response properly", async () =>
    {
        let context = helper.createContext();

        let ev = sinon.mock(Configurapi.Event);

        ev.request = sinon.mock(Configurapi.Request);
        ev.response = sinon.mock(Configurapi.Response);

        ev.response.statusCode = 200;
        ev.response.body = {payload: true};

        logResponseHandler.apply(context, [ev]);

        assert.isTrue(context.emit.called);
        assert.isTrue(context.emit.calledWith(Configurapi.LogLevel.Trace, `Response -  - 200 - {"payload":true}`));
    });

    it("Log a response properly - exclude the payload", async () =>
    {
        let context = helper.createContext();

        let ev = sinon.mock(Configurapi.Event);

        ev.request = sinon.mock(Configurapi.Request);
        ev.response = sinon.mock(Configurapi.Response);

        ev.response.statusCode = 404;
        ev.response.body = {"payload": true, "secret": "abcd", "password": "1234", "public": "data"};

        logResponseHandler.apply(context, [ev, ["secret", "password"]]);

        assert.isTrue(context.emit.called);
        assert.isTrue(context.emit.calledWith(Configurapi.LogLevel.Trace, `Response -  - 404 - {"payload":true,"secret":"*****","password":"*****","public":"data"}`));
    });

    it("Log a response properly - include the payload", async () =>
    {
        let context = helper.createContext();

        let ev = sinon.mock(Configurapi.Event);

        ev.request = sinon.mock(Configurapi.Request);
        ev.response = sinon.mock(Configurapi.Response);

        ev.response.statusCode = 500;
        ev.response.body = {"payload": true};

        logResponseHandler.apply(context, [ev, true]);

        assert.isTrue(context.emit.called);
        assert.isTrue(context.emit.calledWith(Configurapi.LogLevel.Trace, `Response -  - 500 - {"payload":true}`));
    });

    it("Log a response properly - undefined body", async () =>
    {
        let context = helper.createContext();

        let ev = sinon.mock(Configurapi.Event);

        ev.request = sinon.mock(Configurapi.Request);
        ev.response = sinon.mock(Configurapi.Response);

        ev.response.statusCode = 500;
        ev.response.body = undefined;

        logResponseHandler.apply(context, [ev, true]);

        assert.isTrue(context.emit.called);
        assert.isTrue(context.emit.calledWith(Configurapi.LogLevel.Trace, `Response -  - 500 - `));
    });

    it("Log a response properly - undefined response", async () =>
    {
        let context = helper.createContext();

        let ev = sinon.mock(Configurapi.Event);

        ev.request = sinon.mock(Configurapi.Request);
        ev.response = sinon.mock(Configurapi.Response);

        ev.response = undefined;

        logResponseHandler.apply(context, [ev, true]);

        assert.isTrue(context.emit.called);
        assert.isTrue(context.emit.calledWith(Configurapi.LogLevel.Trace, `Response -  - 0 - `));
    });
});
